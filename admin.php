<?php

/**
 * Задача 6. Реализовать вход администратора с использованием
 * HTTP-авторизации для просмотра и удаления результатов.
 **/

// Пример HTTP-аутентификации.
// PHP хранит логин и пароль в суперглобальном массиве $_SERVER.
// Подробнее см. стр. 26 и 99 в учебном пособии Веб-программирование и веб-сервисы.

$user = 'shuimi';
$pass = 'task1pass';
$db = new PDO('mysql:host=localhost;dbname=study', $user, $pass, array(PDO::ATTR_PERSISTENT => true));

try {
    $stmt = $db->prepare("SELECT pwd FROM adminpass WHERE login=:login");
    $result = $stmt->bindParam(':login',$_SERVER['PHP_AUTH_USER']);
    $result = $stmt->execute();
    $adminpwd = $stmt->fetchAll(PDO::FETCH_ASSOC);
}
catch(PDOException $e) {
    print('Error : ' . $e->getMessage());
    exit();
}

if (empty($_SERVER['PHP_AUTH_USER']) || empty($_SERVER['PHP_AUTH_PW']) ||
    !password_verify($_SERVER['PHP_AUTH_PW'], current(current($adminpwd)))) {
  header('HTTP/1.1 401 Unanthorized');
  header('WWW-Authenticate: Basic realm="My site"');
  print('<h1>401 Требуется авторизация</h1>');
  exit();
}

print('
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <title>Lab 6</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css"
          rel="stylesheet"
          integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1"
          crossorigin="anonymous">
    <style>
        .error {
            border:2px solid red;
        }
    </style>
</head>
<body>
    <div class="alert alert-secondary" role="alert">
        Вы успешно авторизовались и видите защищенные паролем данные.
    </div>
        <h1 class="text-center m-5">Админка</h1>
');

// *********
// Здесь нужно прочитать отправленные ранее пользователями данные и вывести в таблицу.
// Реализовать просмотр и удаление всех данных.
// *********


    try {
        $stmt = $db->prepare("SELECT login FROM userspass");
        $result = $stmt->execute(array());
        $users = $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    catch(PDOException $e) {
        print('Error : ' . $e->getMessage());
        exit();
    }

    try {
        $stmt = $db->prepare("SELECT * FROM usersdb");
        $result = $stmt->execute(array());
        $users_data = $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    catch(PDOException $e) {
        print('Error : ' . $e->getMessage());
        exit();
    }

    try {
        $stmt = $db->prepare("SELECT * FROM userspass");
        $result = $stmt->execute(array());
        $users_passes = $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    catch(PDOException $e) {
        print('Error : ' . $e->getMessage());
        exit();
    }

    try {
        $stmt = $db->prepare("SELECT * FROM userspower");
        $result = $stmt->execute(array());
        $users_powers = $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    catch(PDOException $e) {
        print('Error : ' . $e->getMessage());
        exit();
    }

    $intpower = 0;
    try {
        $stmt = $db->prepare("SELECT count(*) FROM userspower WHERE power=:power");
        $stmt -> bindParam(':power', $intpower);
        $result = $stmt->execute();
        $report0 = $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    catch(PDOException $e) {
        print('Error : ' . $e->getMessage());
        exit();
    }
    $intpower = 1;
    try {
        $stmt = $db->prepare("SELECT count(*) FROM userspower WHERE power=:power");
        $stmt -> bindParam(':power', $intpower);
        $result = $stmt->execute();
        $report1 = $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    catch(PDOException $e) {
        print('Error : ' . $e->getMessage());
        exit();
    }
    $intpower = 2;
    try {
        $stmt = $db->prepare("SELECT count(*) FROM userspower WHERE power=:power");
        $stmt -> bindParam(':power', $intpower);
        $result = $stmt->execute();
        $report2 = $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    catch(PDOException $e) {
        print('Error : ' . $e->getMessage());
        exit();
    }

?>

<div class="container">
    <h4 class="text-center m-5">Отчёт о количестве пользователей со способностями</h4>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">Бессмертие</th>
            <th scope="col">Прохождение сквозь стены</th>
            <th scope="col">Левитация</th>
        </tr>
        </thead>
        <tbody>
        <?php
        print('<td>');
        print(current(current($report0)));
        print('</td>');
        print('<td>');
        print(current(current($report1)));
        print('</td>');
        print('<td>');
        print(current(current($report2)));
        print('</td>');
        ?>
        </tbody>
    </table>

</div>

<div class="container">
    <h4 class="text-center m-5">Все данные</h4>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">ID</th>
            <th scope="col">Name</th>
            <th scope="col">Year</th>
            <th scope="col">Sex</th>
            <th scope="col">Email</th>
            <th scope="col">Bio</th>
            <th scope="col">Limb</th>
        </tr>
        </thead>
        <tbody>
<?php
    foreach ($users_data as $cortege){
        print('<tr>');
        foreach ($cortege as $item){
            print('<td>');
            print($item);
            print('</td>');
        }
        print('</tr>');
    }
?>
        </tbody>
    </table>

</div>

<div class="container">
    <table class="table">
        <thead>
        <tr>
            <th scope="col">ID</th>
            <th scope="col">Login</th>
            <th scope="col">Password</th>
        </tr>
        </thead>
        <tbody>
        <?php
        foreach ($users_passes as $cortege){
            print('<tr>');
            foreach ($cortege as $item){
                print('<td>');
                print($item);
                print('</td>');
            }
            print('</tr>');
        }
        ?>
        </tbody>
    </table>

</div>

<div class="container">
    <table class="table">
        <thead>
        <tr>
            <th scope="col">ID</th>
            <th scope="col">Superpower</th>
        </tr>
        </thead>
        <tbody>
        <?php
        foreach ($users_powers as $cortege){
            print('<tr>');
            foreach ($cortege as $item){
                print('<td>');
                print($item);
                print('</td>');
            }
            print('</tr>');
        }
        ?>
        </tbody>
    </table>

</div>

<div class="container">
    <h4 class="text-center m-5">Выбрать редактируемого (удяляемого) пользователя</h4>
    <form method="POST">
        <select name="username" class="form-select" aria-label="select">
            <option selected disabled>Выбор пользователя</option>
            <?php
                foreach($users as $user){
                    print('<option value="');
                    print(current($user));
                    print('">');
                    print(current($user));
                    print('</option>');
                }
            ?>
        </select>
        <br>
        <div class="d-grid gap-2 col-6 mx-auto">
            <div class="btn-group" role="group">
                <button type="submit" name="edit" value="1" class="btn btn-primary">Edit</button>
                <button type="submit" name="delete" value="1"  class="btn btn-primary">Delete</button>
            </div>
        </div>
    </form>
    <br>
    <br>
</div>

<div class="container">
    <h4 class="text-center m-5">Добавить пользователя для теста</h4>
    <form method="POST">
        <div class="d-grid gap-2 col-6 mx-auto">
            <button class="btn btn-light" type="submit" name="show_injection_form" value="1">Показать форму</button>
        </div>
    </form>
    <br>
</div>

<?php
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if($_POST['edit']){
        $edit_login = $_POST['username'];

        try {
            $stmt = $db->prepare("SELECT id FROM userspass WHERE login=:login");
            $stmt -> bindParam(':login', $edit_login);
            $stmt->execute();
            $user_id = current(current($stmt->fetchAll(PDO::FETCH_ASSOC)));
        }
        catch(PDOException $e) {
            print('Error : ' . $e->getMessage());
            exit();
        }

        try {
            $stmt = $db->prepare("SELECT * FROM usersdb WHERE id=:id");
            $stmt -> bindParam(':id', $user_id);
            $result = $stmt->execute();
            $users_data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        }
        catch(PDOException $e) {
            print('Error : ' . $e->getMessage());
            exit();
        }

        try {
            $stmt = $db->prepare("SELECT * FROM userspower WHERE id=:id");
            $stmt -> bindParam(':id', $user_id);
            $result = $stmt->execute();
            $user_power = $stmt->fetchAll(PDO::FETCH_ASSOC);
        }
        catch(PDOException $e) {
            print('Error : ' . $e->getMessage());
            exit();
        }

        $values = array();
        $values['name'] = $users_data[0]['name'];
        $values['email'] = $users_data[0]['email'];
        $values['year'] = $users_data[0]['year'];
        $values['power'] = $user_power[0]['power'];
        $values['sex'] = $users_data[0]['sex'];
        $values['limb'] = $users_data[0]['limb'];
        $values['bio'] = $users_data[0]['bio'];

        ?>

<div class="container">
    <h4 class="text-center m-5">Редактировать данные для <?php print $_POST['username'];?></h4>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">ID</th>
            <th scope="col">Name</th>
            <th scope="col">Year</th>
            <th scope="col">Sex</th>
            <th scope="col">Email</th>
            <th scope="col">Bio</th>
            <th scope="col">Limb</th>
            <th scope="col">Superpower</th>
        </tr>
        </thead>
        <tbody>
        <?php
        foreach ($users_data as $cortege){
            print('<tr>');
            foreach ($cortege as $item){
                print('<td>');
                print($item);
                print('</td>');
            }
            print('<td>');
            print($user_power[0]['power']);
            print('</td>');
            print('</tr>');
        }
        ?>
        </tbody>
    </table>

</div>

<div class="container">
    <div class="container-sm theme-list py-3 pl-0 mb-3">
        <div class="d-flex flex-column align-items-center">

            <form class="d-block p-2" action="" method="POST">

                <div class="form-floating mb-3">
                    <input name="name"
                           class="form-control"
                           value="<?php print $values['name']; ?>"
                           type="text"
                    />
                    <label for="fio" class="form-label">Имя</label>
                </div>

                <div class="form-floating mb-3">
                    <input name="email"
                           class="form-control"
                           value="<?php print $values['email']; ?>"
                           value="sample@example.com"
                           type="text"
                    />
                    <label for="email" class="form-label">Email</label>
                </div>

                <div class="mb-3">
                    <label for="year" class="form-label">Дата рождения</label>
                    <select class="form-control" name="year"
                            value="<?php print $values['year']; ?>">
                        <option value="выбрать...">Выбрать</option>
                        <?php for($i = 1900; $i < 2021; $i++) {?>
                            <option <?php if ($values['year']==$i){print 'selected="selected"';} ?> value="<?php print $i; ?>"><?= $i; ?></option>
                        <?php }?>
                    </select>
                </div>

                <div class="mb-3">
                    <label>Пол</label>

                    <br>
                    <input type="radio"
                           name="sex"
                        <?php if ($values['sex']==0){print 'checked';} ?>
                           value="0"
                    /> Male

                    <input type="radio"
                        <?php if ($values['sex']==1){print 'checked';} ?>
                           name="sex"
                           value="1"
                    /> Female

                </div>

                <div class="mb-3">
                    <label class="form-label">Число конечностей</label>
                    <br>
                    <input type="radio"
                        <?php if ($values['limb']==0 || $values['limb']==1){print 'checked';} ?>
                           name="limb"
                           value="1"
                    />
                    <label class="form-check-label">1</label>
                    <input type="radio"
                        <?php if ($values['limb']==2){print 'checked';} ?>
                           name="limb"
                           value="2" />
                    <label class="form-check-label">2</label>
                    <input type="radio"
                        <?php if ($values['limb']==3){print 'checked';} ?>
                           name="limb"
                           value="3"
                    />
                    <label class="form-check-label">3</label>
                    <input type="radio"
                        <?php if ($values['limb']==4){print 'checked';} ?>
                           name="limb"
                           value="4" />
                    <label class="form-check-label">4</label>
                </div>

                <div class="mb-3">
                    <label class="form-label">Сверхсособности</label>
                    <select class="form-control" name="power[]" multiple="multiple">
                        <option
                            <?php if ($values['power'] == 0){print 'selected="selected"';} ?>
                            value="0">Бессмертие
                        </option>
                        <option
                            <?php if ($values['power'] == 1){print 'selected="selected"';} ?>
                            value="1">Прохождение сквозь стены
                        </option>
                        <option
                            <?php if ($values['power'] == 2){print 'selected="selected"';} ?>
                            value="2">Левитация
                        </option>
                    </select>
                </div>

                <div class="mb-3 form-floating">
                    <textarea class="form-control"
                              name="bio"
                              placeholder="Your biography"
                              style="height: 100px; width: 420px;"><?php print $values['bio']; ?></textarea>
                    <label class="form-label">Биография</label>
                </div>

                <div class="mb-3 form-check">
                    <input type="checkbox" name="check" required/>
                    <label class="form-check-label">Даю согласие на всё!</label>
                </div>

                <div class="d-grid gap-2 col-6 mx-auto">
                    <button class="btn btn-light" type="submit" name="correct" value="<?php print $user_id;?>">Работать!</button>
                </div>

            </form>

        </div>
    </div>
</div>>

        <?php
    };
    if($_POST['delete']){

        $delete_login = $_POST['username'];

        try {
            $stmt = $db->prepare("SELECT id FROM userspass WHERE login=:login");
            $stmt -> bindParam(':login', $delete_login);
            $stmt->execute();
            $delete_id = current(current($stmt->fetchAll(PDO::FETCH_ASSOC)));

            $stmt1 = $db->prepare("DELETE FROM userspass where id=:id");
            $stmt1 -> bindParam(':id', $delete_id);
            $stmt1 -> execute();

            $stmt2 = $db->prepare("DELETE FROM usersdb where id=:id");
            $stmt2 -> bindParam(':id', $delete_id);
            $stmt2 -> execute();

            $stmt3 = $db->prepare("DELETE FROM userspower where id=:id");
            $stmt3 -> bindParam(':id', $delete_id);
            $stmt3 -> execute();
        }
        catch(PDOException $e) {
            print('Error : ' . $e->getMessage());
            exit();
        }
        print('
        <meta http-equiv="refresh" content="0">
        ');
    };
    if(!empty($_POST['correct'])){

        try {
            $stmt1 = $db->prepare("UPDATE usersdb SET name=:name, year=:year, sex=:sex, email=:email, bio=:bio, limb=:limb WHERE id =:id");
            $stmt1 -> bindParam(':name', $_POST['name']);
            $stmt1 -> bindParam(':year', $_POST['year']);
            $stmt1 -> bindParam(':sex', $_POST['sex']);
            $stmt1 -> bindParam(':email', $_POST['email']);
            $stmt1 -> bindParam(':bio', $_POST['bio']);
            $stmt1 -> bindParam(':limb', $_POST['limb']);
            $stmt1 -> bindParam(':id', intval($_POST['correct']));
            $stmt1 -> execute();
        }
        catch(PDOException $e) {
            print('Error : ' . $e->getMessage());
            exit();
        }

        try {
            $temp_power = intval(current($_POST['power']));
            $temp_user_id = intval($_POST['correct']);
            $stmt4 = $db->prepare("UPDATE userspower SET id=:user_id, power=:power WHERE id =:user_id");
            $stmt4 -> execute(['user_id'=>$temp_user_id, 'power'=>$temp_power]);
        }
        catch(PDOException $e) {
            print('Error : ' . $e->getMessage());
            exit();
        }
        print('
        <meta http-equiv="refresh" content="0">
        ');
    };
    if($_POST['injection']){

        $delete_login = $_POST['username'];

        try {
            $stmt = $db->prepare("INSERT INTO usersdb (name,year,sex,email,bio,limb) VALUES (:name,:year,:sex,:email,:bio,:limb)");
            $stmt -> execute(
                ['name'=>$_POST['name'], 'year'=>$_POST['year'], 'sex'=>$_POST['sex'], 'email'=>$_POST['email'], 'bio'=>$_POST['bio'], 'limb'=>$_POST['limb']]
            );
            $last_id = $db->lastInsertId();

            $stmt1 = $db->prepare("INSERT INTO userspower (id, power) VALUES (:id,:power)");
            $stmt1 -> bindParam(':id', intval($last_id));
            $stmt1 -> bindParam(':power', intval($_POST['power']));
            $stmt1 -> execute();

            $stmt4 = $db->prepare("INSERT INTO userspass (id, login, pwd) VALUES (:id,:login, :pwd)");
            $stmt4 -> bindParam(':id', intval($last_id));
            $stmt4 -> bindParam(':login', $_POST['login']);
            $stmt4 -> bindParam(':pwd', password_hash($_POST['password'], PASSWORD_DEFAULT));
            $stmt4 -> execute();
        }
        catch(PDOException $e) {
            print('Error : ' . $e->getMessage());
            exit();
        }

        print('
        <meta http-equiv="refresh" content="0">
        ');
    };
    if($_POST['show_injection_form']){
        print('
        
<div class="container">
    <h4 class="text-center m-5">Добавить пользователя для теста</h4>
    <div class="container-sm theme-list py-3 pl-0 mb-3">
        <div class="d-flex flex-column align-items-center">

            <form class="d-block p-2" action="" method="POST">

                <div class="row">
                    <div class="col">
                        <div class="form-floating mb-3">
                            <input name="name" class="form-control" type="text"/>
                            <label class="form-label">Имя</label>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-floating mb-3">
                            <input name="email" class="form-control" type="text"/>
                            <label class="form-label">Email</label>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <div class="mb-3">
                            <label>Пол</label>
                            <br>
                            <input type="radio" name="sex" value="0"/> Male
                            <input type="radio" name="sex" value="1"/> Female
                        </div>
                    </div>
                    <div class="col">
                        <div class="mb-3">
                            <label class="form-label">Число конечностей</label>
                            <br>
                            <input type="radio" name="limb" value="1"/>
                            <label class="form-check-label">1</label>
                            <input type="radio" name="limb" value="2"/>
                            <label class="form-check-label">2</label>
                            <input type="radio" name="limb" value="3"/>
                            <label class="form-check-label">3</label>
                            <input type="radio" name="limb" value="4"/>
                            <label class="form-check-label">4</label>
                        </div>
                    </div>
                    <div class="col">
                        <div class="mb-3">
                            <select class="form-control" name="year">
                                <option value="выбрать...">Выбрать</option>');
                            for($i = 1900; $i < 2021; $i++) {
                                print('<option value="'.$i.'">'.$i.'</option>');
                            }
                            print('
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <div class="form-floating mb-3">
                            <input name="login" class="form-control" type="text"/>
                            <label class="form-label">Логин</label>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-floating mb-3">
                            <input name="password" class="form-control" type="text"/>
                            <label class="form-label">Пароль</label>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <div class="mb-3 form-floating">
                            <textarea class="form-control" name="bio" placeholder="Your biography" style="height: 100px; width: 420px;"></textarea>
                            <label class="form-label">Биография</label>
                        </div>
                    </div>
                    <div class="col">
                        <div class="mb-3">
                            <select class="form-control" name="power" multiple="multiple">
                                <option value="0">Бессмертие</option>
                                <option value="1">Прохождение сквозь стены</option>
                                <option value="2">Левитация</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="d-grid gap-2 col-6 mx-auto">
                    <button class="btn btn-light" type="submit" name="injection" value="1">Добавить!</button>
                </div>

            </form>

        </div>
    </div>
</div>

        ');
    };
}
?>
    <footer>
        <h2 class="text-center m-3">
            Шустов В.Д. 25/2 2020-2021
        </h2>
    </footer>
</body>